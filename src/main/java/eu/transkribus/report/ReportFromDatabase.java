package eu.transkribus.report;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;
import javax.persistence.EntityNotFoundException;

import eu.transkribus.core.model.beans.job.enums.JobImpl;
import eu.transkribus.persistence.dao.TrpUserDao;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.core.io.util.TrpProperties;
import eu.transkribus.core.model.beans.TrpCreditTransaction;
import eu.transkribus.core.model.beans.auth.TrpUser;
import eu.transkribus.core.util.CoreUtils;
import eu.transkribus.persistence.DbConnection;
import eu.transkribus.persistence.DbConnection.DbConfig;
import eu.transkribus.persistence.io.LocalStorage;
import eu.transkribus.persistence.util.MailUtils;

/*
 * 
 * Mailing report 
 * Florian 
 * 
 */

public class ReportFromDatabase {
	private final static Logger logger = LoggerFactory.getLogger(ReportFromDatabase.class);
	private final static String HTR_MODULE = "PyLaiaDecodingJob";
	private final static String OCR_MODULE = "FinereaderOcrJob";
	private final static String LA_MODULE = "CITlabAdvancedLaJobMultiThread";
	private final static String UPLOAD = "UploadImportJob";

	private final static String LITECLIENT = "transkribus-webui";
	private final static String EXPERTCLIENT = "transkribus-swt-gui";

	private int imagesUploaded = 0;
	private int docScanUploaded = 0;
	private int countJobs = 0;
	private int countNewUsers = 0;
	private List<JSONObject> mostActiveUsers = new ArrayList<>();
	private List<JSONObject> mostActiveDocScan = new ArrayList<>();
	private List<JSONObject> mostActiveUsersSave = new ArrayList<>();
	private List<JSONObject> trainingTime = new ArrayList<>();
	private List<JSONObject> htrRunTime = new ArrayList<>();
	private List<JSONObject> ocrRunTime = new ArrayList<>();
	private List<JSONObject> laRunTime = new ArrayList<>();

	private List<JSONObject> uploadLite = new ArrayList<>();
	private List<JSONObject> uploadGui = new ArrayList<>();
	private List<JSONObject> htrLite = new ArrayList<>();
	private List<JSONObject> htrGui = new ArrayList<>();

	private String totalTrainingTime;
	private String totalHtrTime;
	private String totalOcrTime;
	private String totalLaTime;
	private String htrModelsCreated;
	private String runningTotals = "Running Totals : ";
	private int countActiveUsers = 0;
	private int countTotalSaves = 0;
	private int countCreatedDocs = 0;
	private int countDuplDocs = 0;
	private int countExpDocs = 0;
	private int countDelDocs = 0;
	private int countLayoutAnalysis = 0;
	private int countHTR = 0;
	private int countKWS = 0;
	int countShopOrders = 0;
	int countCreditPackages = 0;
	List<Pair<Integer, String>> creditProductRanking;
	private List<TrpCreditTransaction> freeCreditConsumption;
	private List<TrpCreditTransaction> paidCreditConsumption;
	double countCredits = 0;
	private Set<Integer> pageIndices = new HashSet<>();
	private JSONObject jo = new JSONObject();

	public void sendReportMail(List<String> recipients, File[] files, Date from, Date to, String timespanLabel) {


		String trainingString = formatJsonArray(trainingTime);
		String htrRunString = formatJsonArray(htrRunTime);
		String ocrRunString = formatJsonArray(ocrRunTime);
		String laString = formatJsonArray(laRunTime);
		String activeUsersString = formatJsonArray(mostActiveUsers);
		String activeSavesString = formatJsonArray(mostActiveUsersSave);
		String activeDocScanString = formatJsonArray(mostActiveDocScan);
		DateFormat df = CoreUtils.newDateFormatUserFriendly();

		String messageText = "This is the latest report from " + new java.sql.Date(to.getTime())
				+ " with detailed user data over a period from " + df.format(from) + " to " + df.format(to) + "\n" + "\nNew Users : "
				+ countNewUsers + " \n" + "Active Users / Unique Logins : " + countActiveUsers + " \n"
				+ "Jobs processed in total: " + countJobs + " \n" + "Created Documents: " + countCreatedDocs + " \n"
				+ "Duplicated Documents: " + countDuplDocs + " \n" + "Export Documents: " + countExpDocs + " \n"
				+ "KWS searches: " + countKWS + " \n"
				+ "Layout Analysis Jobs: " + countLayoutAnalysis + " \n" + "PyLaia HTR Jobs : " + countHTR + " \n"
				+ "HTR Models created: " + htrModelsCreated + " \n" + "\nImage Uploads : " + imagesUploaded + "\n"
				+ activeUsersString + "\n"
				+ "\nDocScan Uploads : " + (docScanUploaded < 1 ? "N/A" : docScanUploaded) + "\n" + activeDocScanString
				+ "\n" + "\nSave Actions : " + countTotalSaves + " \n" + activeSavesString + "\n"
				+ "\nPyLaia Training Runtime : " + totalTrainingTime + " \n" + trainingString + "\n" + "\nPyLaia HTR Runtime : "
				+ totalHtrTime + "\n" + htrRunString + "\n" + "\nOCR Runtime : " + totalOcrTime + " \n" + ocrRunString
				+ "\n" + "\nLA Runtime : " + totalLaTime + " \n" + laString + "\n" + "\n" + runningTotals
				+ formatCreditConsumption(freeCreditConsumption, paidCreditConsumption) + "\n\nWebshop Statistics:\n"
				+ "\nNr. of orders processed: " + countShopOrders + "\nNr. of purchased packages: "
				+ countCreditPackages + "\nNr. of credits sold: " + countCredits
				+ formatTopProducts(creditProductRanking);

		for (String mailTo : recipients) {
			try {
				MailUtils.DEFAULT_MAIL_SERVER.sendMail(mailTo, timespanLabel + " report from " + new java.sql.Date(to.getTime()),
						messageText, files, "", false, false);
			} catch (MessagingException e) {
				logger.error("Could not send mail to " + mailTo, e);
			}
		}
		writeToJSON(from, to, timespanLabel);

	}

	private static String formatJsonArray(List<JSONObject> jsonList) {
		return jsonList.subList(0, Math.min(5, jsonList.size())).toString().replace("{", "\n")
				.replaceAll("\\{([^}]*?)\\}", "");
	}

	private void writeToJSON(Date from, Date to, String timespanLabel) {
		jo.put("date", from);
		jo.put("newUser", countNewUsers);
		jo.put("countShopOrders", countShopOrders);
		jo.put("countCreditPackages", countCreditPackages);
		jo.put("countCredits", countCredits);
		jo.put("creditProductRanking", creditProductRanking);
		jo.put("activeUser", countActiveUsers);
		jo.put("jobs", countJobs);
		jo.put("createDocs", countCreatedDocs);
		jo.put("duplicatedDocs", countDuplDocs);
		jo.put("exportDocs", countExpDocs);
		jo.put("deletedDocs", countDelDocs);
		jo.put("kwsSearch", countKWS);
		jo.put("layoutAnalysis", countLayoutAnalysis);
		jo.put("htrJobs", countHTR);
		jo.put("images", imagesUploaded);
		jo.put("imagesUsers", mostActiveUsers);
		jo.put("docScan", docScanUploaded);
		jo.put("docScanUsers", mostActiveDocScan);
		jo.put("saves", countTotalSaves);
		jo.put("saveUsers", mostActiveUsersSave);
		jo.put("trainingTime", trainingTime);
		jo.put("htrRunTime", htrRunTime);
		jo.put("ocrRunTime", ocrRunTime);
		jo.put("laRunTime", laRunTime);

		jo.put("uploadLite", uploadLite);
		jo.put("uploadGui", uploadGui);
		jo.put("htrLite", htrLite);
		jo.put("htrGui", htrGui);
		
		jo.put("freeCreditConsumption", freeCreditConsumption);
		jo.put("paidCreditConsumption", paidCreditConsumption);

		File reportPath;
		//Quickfix to allow generating report files for test env without overwriting prod data. Test API will not provide any data like this though.
		if(DbConfig.Prod.equals(DbConnection.getConfig())) {
			reportPath = LocalStorage.getReportStorage();
		} else {
			reportPath = new File("/tmp/test-reports");
		}

		final String subDir = timespanLabel.toLowerCase();

		File reportParent = new File(reportPath, subDir);
		if(!reportParent.isDirectory()) {
			if(!reportParent.mkdirs()) {
				logger.warn("Could not write JSON report file as target directory was not writable: {}", reportParent.getAbsolutePath());
				return;
			}
		}
		File reportFile = new File(reportParent, new java.sql.Date(to.getTime()) + ".json");
		if(reportFile.isFile()) {
			//FIXME don't overwrite, otherwise testing the tool would always delete stuff. 
			//Add path as argument and use this path only if argument is not set?
			logger.warn("Not overwriting existing report file at {}", reportFile.getAbsolutePath());
			logger.info("Report JSON:\n{}", jo);
			return;
		}
		File latestLink = new File(reportParent, "latest.json");
		
		try (FileWriter file = new FileWriter(reportFile)) {
			file.write(jo.toString());
			file.flush();
			logger.info("Report JSON written to {}", reportFile.getAbsolutePath());
			if(latestLink.exists()) {
				latestLink.delete();
			}
			Files.createSymbolicLink(latestLink.toPath(), reportFile.toPath());
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
	}

	private static String formatTopProducts(List<Pair<Integer, String>> creditProductRanking) {
		String msg = "\n\nTop Products:\n" + "\nPurchases\tProduct"
				+ "\n---------------------------------------------------------\n";
		for (Pair<Integer, String> item : creditProductRanking) {
			if (item.getKey() < 1) {
				continue;
			}
			msg += item.getKey() + "\t\t" + item.getValue() + "\n";
		}
		return msg;
	}

	private static String formatCreditConsumption(List<TrpCreditTransaction> freeCreditConsumption,
			List<TrpCreditTransaction> paidCreditConsumption) {
		final int colSize = 20;
		String msg = "\n\nCredit Consumption:\n";
		if (CollectionUtils.isEmpty(paidCreditConsumption) && CollectionUtils.isEmpty(paidCreditConsumption)) {
			msg += "NONE";
			return msg;
		}
		// rightpad values to consistent column sizes as using tabs produced a mess here
		msg += "\n" + StringUtils.rightPad("Type", colSize) + StringUtils.rightPad("Technology", colSize)
				+ StringUtils.rightPad("Credits Used", colSize) + StringUtils.rightPad("Pages Processed", colSize)
				+ "\n" + StringUtils.rightPad("", colSize * 4, "-") + "\n" + "Free Of Charge\n";
		for (TrpCreditTransaction t : freeCreditConsumption) {
			msg += StringUtils.rightPad("", colSize) + StringUtils.rightPad(t.getDescription(), colSize)
					+ StringUtils.rightPad("" + t.getCreditValue(), colSize)
					+ StringUtils.rightPad("" + t.getNrOfPages(), colSize) + "\n";
		}
		msg += "Paid\n";
		for (TrpCreditTransaction t : paidCreditConsumption) {
			msg += StringUtils.rightPad("", colSize) + StringUtils.rightPad(t.getDescription(), colSize)
					+ StringUtils.rightPad("" + t.getCreditValue(), colSize)
					+ StringUtils.rightPad("" + t.getNrOfPages(), colSize) + "\n";
		}
		return msg;
	}

	public void emailMessage(Date from, Date to) throws SQLException {

		TrpUserDao dao = new TrpUserDao();
		try {
			List<TrpUser> userList = dao.getUsersByDateRange(from, to);
			countNewUsers = userList.size();
			final int totalUser = dao.countTrpUsers(null, null, null, false, false, false);
			final int totalActiveUser = dao.countTrpUsers(null, null, null, false, false, true);
			runningTotals += "\nTotal Users : " + totalUser
					+ "\nTotal Active Users (Activated account via registration link) : " + totalActiveUser;
			jo.put("totalUsers", totalUser);
			jo.put("totalActiveUsers", totalActiveUser);
		} catch (EntityNotFoundException | SQLException e) {
			logger.error(e.getMessage(), e);
		}

		String sqlLogins = "select count(distinct user_id) from actions where type_id = 2 and time between ? and ?";
		countActiveUsers = queryForDecimalValue(sqlLogins, from, to).intValue();

		String sqlLA = "select count(*) from jobs where (module_name like 'NcsrLaModule' or module_name like 'LaModule') and starttime between ? and ?";
		countLayoutAnalysis = queryForDecimalValue(sqlLA, from.getTime(), to.getTime()).intValue();

		String sqlHTR = "select count(*) from jobs where module_name like 'PyLaiaModule' and starttime between ? and ?";
		countHTR = queryForDecimalValue(sqlHTR, from.getTime(), to.getTime()).intValue();

		String sqlKWS = "select count(*) from jobs where module_name like 'KwsModule' and starttime between ? and ?";
		countKWS = queryForDecimalValue(sqlKWS, from.getTime(), to.getTime()).intValue();

		String sqlCreatedDoc = "select count(*) from jobs where type like 'Create Document' and starttime between ? and ?";
		countCreatedDocs = queryForDecimalValue(sqlCreatedDoc, from.getTime(), to.getTime()).intValue();

		String sqlDuplDoc = "select count(*) from jobs where type like 'Duplicate Document' and starttime between ? and ?";
		countDuplDocs = queryForDecimalValue(sqlDuplDoc, from.getTime(), to.getTime()).intValue();

		String sqlExpDoc = "select count(*) from jobs where type like 'Export Document' and starttime between ? and ?";
		countExpDocs = queryForDecimalValue(sqlExpDoc, from.getTime(), to.getTime()).intValue();

		String sqlImages = "select count(*) from images where images.created between ? and ?";
		imagesUploaded = queryForDecimalValue(sqlImages, from, to).intValue();

		String sqlJobs = "select count(*) from jobs where starttime between ? and ?";
		countJobs = queryForDecimalValue(sqlJobs, from.getTime(), to.getTime()).intValue();

		String sqlSavesTotal = "select count(*) from actions where type_id = 1 and time between ? and ?";
		countTotalSaves = queryForDecimalValue(sqlSavesTotal, from, to).intValue();

		//the queries for docscan uploads exceed the DB connection timeout of 30 mintues in prod! do not execute
		String sqlDocScanImages = "select count(*) from images \n"
				+ "join pages on IMAGES.IMAGE_ID = PAGES.IMAGE_ID join jobs on PAGES.DOCID = jobs.DOCID join session_history on jobs.SESSION_HISTORY_ID = session_history.SESSION_HISTORY_ID \n"
				+ "where session_history.USERAGENT like '%Android%' and images.CREATED between ? and ?";
		docScanUploaded = -1; //queryForDecimalValue(sqlDocScanImages, from, to).intValue();
		mostActiveDocScan = new ArrayList<>(0); //queryForMostActiveDocScan(from, to);

		countDelDocs = 0; //DeleteDocJob is no longer in use

		pageIndices = new HashSet<>(); // "pageIndices" is never used in reports and it looks buggy anyway. Was = queryForPyLaiaPageIndices(from, to);

		mostActiveUsers = queryForMostActiveUsers(from, to);

		mostActiveUsersSave = queryForMostActiveUsersSave(from, to);

	}

	private List<JSONObject> queryForMostActiveDocScan(Date from, Date to) throws SQLException {
		List<JSONObject> mostActiveDocScan = new ArrayList<>();
		try (Connection conn = DbConnection.getConnection()) {
			String sqlDocScanUsers = "select jobs.USERID, count (distinct images.image_id) from images \n"
					+ "join pages on IMAGES.IMAGE_ID = PAGES.IMAGE_ID join jobs on PAGES.DOCID = jobs.DOCID join session_history on jobs.SESSION_HISTORY_ID = session_history.SESSION_HISTORY_ID \n"
					+ "where session_history.USERAGENT like '%Android%' and images.CREATED between ? and ? \n"
					+ "group by jobs.USERID order by count(distinct images.image_id) desc";
			PreparedStatement prep16 = conn.prepareStatement(sqlDocScanUsers);
			prep16.setDate(1, new java.sql.Date(from.getTime()));
			prep16.setDate(2, new java.sql.Date(to.getTime()));
			ResultSet rs16 = prep16.executeQuery();
			while (rs16.next()) {
				mostActiveDocScan.add(new JSONObject("{\"user\":\"" + rs16.getString(1) + "\",\"images\":"
						+ rs16.getInt(2) + "}"));
			}
		}
		return mostActiveDocScan;
	}

	private List<JSONObject> queryForMostActiveUsersSave(Date from, Date to) throws SQLException {
		List<JSONObject> mostActiveUsersSave = new ArrayList<>();
		try (Connection conn = DbConnection.getConnection()) {
			String sqlMostSaves = "select user_name, count(type_id) from actions where type_id = 1 and time between ? and ? group by user_name order by count (type_id) desc";
			PreparedStatement prep3 = conn.prepareStatement(sqlMostSaves);
			prep3.setDate(1, new java.sql.Date(from.getTime()));
			prep3.setDate(2, new java.sql.Date(to.getTime()));
			ResultSet rs3 = prep3.executeQuery();
			while (rs3.next()) {
				mostActiveUsersSave.add(new JSONObject("{\"user\":\"" + rs3.getString(1) + "\",\"saves\":"
						+ rs3.getInt(2) + "}"));
			}
		}
		return mostActiveUsersSave;
	}

	private List<JSONObject> queryForMostActiveUsers(Date from, Date to) throws SQLException {
		List<JSONObject> mostActiveUsers = new ArrayList<>();
		try (Connection conn = DbConnection.getConnection()) {
			String sqlMostActive = "select uploader, count (distinct images.image_id) from images join pages on IMAGES.IMAGE_ID = PAGES.IMAGE_ID join doc_md on PAGES.DOCID = DOC_MD.DOCID where images.created between ? and ? group by uploader order by count(distinct images.image_id) desc";
			PreparedStatement prep2 = conn.prepareStatement(sqlMostActive);
			prep2.setDate(1, new java.sql.Date(from.getTime()));
			prep2.setDate(2, new java.sql.Date(to.getTime()));
			ResultSet rs2 = prep2.executeQuery();
			while (rs2.next()) {
				final String user = rs2.getString(1);
				final int nrOfImages = rs2.getInt(2);
				String jsonString = "{\"user\":\"" + user + "\",\"images\":"
						+ nrOfImages + "}";
				try {
					mostActiveUsers.add(new JSONObject(jsonString));
				} catch (JSONException e) {
					logger.error("Could not create JSON object from string: {}", jsonString, e);
				}
			}
		}
		return mostActiveUsers;
	}

	/** @deprecated the code looks incomplete. Just moved it to this method for brevity... */
	private Set<Integer> queryForPyLaiaPageIndices(Date from, Date to) throws SQLException {
		Set<Integer> pageIndices = new HashSet<>();
		try (Connection conn = DbConnection.getConnection()) {
			String sqlHtrPages = "select pages,docid from jobs where module_name like 'PyLaiaModule' and starttime between ? and ?";
			PreparedStatement prep11 = conn.prepareStatement(sqlHtrPages);
			prep11.setLong(1, from.getTime());
			prep11.setLong(2, to.getTime());
			ResultSet rs11 = prep11.executeQuery();
			while (rs11.next()) {
				if (rs11.getString("pages") != null) {
					try {
						//FIXME each result set entry overwrites the previous value here?
						pageIndices = CoreUtils.parseRangeListStr(rs11.getString("pages"), rs11.getInt("docid"));
					} catch (IOException e) {
						logger.error(e.getMessage(), e);
					}
				}
			}
		}
		return pageIndices;
	}

	public void getJobTime(Date from, Date to, String moduleName) throws SQLException {

		String SQL = "select userid, sum(endtime - starttime) as job_runtime, sum(total_work) as total_work from jobs\n"
				+ "where job_impl like ? and STATE like 'FINISHED' and starttime between ? and ?\n" + "group by  userid\n"
				+ "order by sum(endtime - starttime) DESC\n";
		try (Connection conn = DbConnection.getConnection()) {
			PreparedStatement prep = conn.prepareStatement(SQL);
			prep.setString(1, moduleName);
			prep.setLong(2, from.getTime());
			prep.setLong(3, to.getTime());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				switch (moduleName) {
					case HTR_MODULE:

						htrRunTime.add(new JSONObject("{\"user\":\"" + rs.getString("userid") + "\",\"time\":\""
								+ hourFormat(rs.getLong("job_runtime")) + "\",\"pages\": "
								+ rs.getLong("total_work") + "}"));
						break;
					case OCR_MODULE:
						ocrRunTime.add(new JSONObject("{\"user\":\"" + rs.getString("userid") + "\",\"time\":\""
								+ hourFormat(rs.getLong("job_runtime")) + "\"}"));
						break;
					case LA_MODULE:
						laRunTime.add(new JSONObject("{\"user\":\"" + rs.getString("userid") + "\",\"time\":\""
								+ hourFormat(rs.getLong("job_runtime")) + "\",\"pages\": "
								+ rs.getLong("total_work") + "}"));
				}
			}
		}
	}

	public void getTotalJobTime(Date from, Date to, String moduleName) throws SQLException {

		String SQL = "select sum(endtime-starttime) as job_runtime, sum(total_work) as total_work from jobs\n"
				+ "where job_impl like ? and STATE like 'FINISHED' and starttime between ? and ?";
		try (Connection conn = DbConnection.getConnection()) {
			PreparedStatement prep = conn.prepareStatement(SQL);
			prep.setString(1, moduleName);
			prep.setLong(2, from.getTime());
			prep.setLong(3, to.getTime());
			ResultSet rs = prep.executeQuery();

			while (rs.next()) {
				switch (moduleName) {
					case HTR_MODULE:
						totalHtrTime = " " + hourFormat(rs.getLong("job_runtime")) + " | Pages : "
								+ rs.getInt("total_work");
						jo.put("totalHtrTime", hourFormat(rs.getLong("job_runtime")));
						jo.put("totalHtrPages", rs.getInt("total_work"));
						break;
					case OCR_MODULE:
						totalOcrTime = " " + hourFormat(rs.getLong("job_runtime"));
						break;
					case LA_MODULE:
						totalLaTime = " " + hourFormat(rs.getLong("job_runtime")) + " | Pages : "
								+ rs.getInt("total_work");
						jo.put("totalOcrTime", hourFormat(rs.getLong("job_runtime")));
						jo.put("totalOcrPages", rs.getInt("total_work"));
				}
			}
		}
	}

	public void getClientJobTime(Date from, Date to, String moduleName, String clientId)
			throws SQLException {

		String SQL = "select endtime-starttime as job_runtime, userid, total_work, ip\n"
				+ "from jobs join session_history on jobs.session_history_id = session_history.session_history_id\n"
				+ "where job_impl like ? and STATE like 'FINISHED' and starttime between ? and ? and client_id like ?";
		try (Connection conn = DbConnection.getConnection()) {
			PreparedStatement prep = conn.prepareStatement(SQL);
			prep.setString(1, moduleName);
			prep.setLong(2, from.getTime());
			prep.setLong(3, to.getTime());
			prep.setString(4, clientId);
			ResultSet rs = prep.executeQuery();

			while (rs.next()) {
				switch (moduleName) {
					case HTR_MODULE:
						if (clientId.equals(LITECLIENT)) {
							htrLite.add(new JSONObject("{\"user\":\"" + rs.getString("userid") + "\",\"time\":\""
									+ hourFormat(rs.getLong("job_runtime")) + "\",\"pages\": \""
									+ rs.getLong("total_work") + "\",\"ip\": \"" + rs.getString("ip") + "\"}"));

						} else {
							htrGui.add(new JSONObject("{\"user\":\"" + rs.getString("userid") + "\",\"time\":\""
									+ hourFormat(rs.getLong("job_runtime")) + "\",\"pages\": \""
									+ rs.getLong("total_work") + "\",\"ip\": \"" + rs.getString("ip") + "\"}"));

						}
						break;
					case UPLOAD:
						if (clientId.equals(LITECLIENT)) {
							uploadLite.add(new JSONObject("{\"user\":\"" + rs.getString("userid") + "\",\"time\":\""
									+ hourFormat(rs.getLong("job_runtime")) + "\",\"ip\": \"" + rs.getString("ip")
									+ "\"}"));
						} else {
							uploadGui.add(new JSONObject("{\"user\":\"" + rs.getString("userid") + "\",\"time\":\""
									+ hourFormat(rs.getLong("job_runtime")) + "\",\"ip\": \"" + rs.getString("ip")
									+ "\"}"));
						}
						break;
				}
			}
		}
	}

	public void getTrainingTime(Date from, Date to) throws SQLException {

		String SQL = "select userid, sum(endtime - starttime) as job_runtime, sum(j.total_work) from jobs j \n"
				+ "where j.state like 'FINISHED' and starttime between ? and ? and "
				+ "(j.JOB_IMPL like '" + JobImpl.PyLaiaTrainingJob + "' or j.JOB_IMPL like '" + JobImpl.PyLaiaTrainingJobLarge + "')\n"
				+ "group by  userid\n" + "order by sum(endtime - starttime) DESC\n";
//						"FETCH FIRST 10 ROWS ONLY";
		try (Connection conn = DbConnection.getConnection()) {
			PreparedStatement prep = conn.prepareStatement(SQL);
			prep.setLong(1, from.getTime());
			prep.setLong(2, to.getTime());
			ResultSet rs = prep.executeQuery();

			int i = 0;
			while (rs.next()) {
				trainingTime.add(new JSONObject("{\"user\":\"" + rs.getString("userid") + "\",\"time\":\""
						+ hourFormat(rs.getLong("job_runtime")) + "\"}"));
				i++;
			}
		}
	}

	public void getTrainingTotalTime(Date from, Date to) throws SQLException {

		String SQL = "select sum(endtime - starttime) as job_runtime, sum(total_work) as total_work from jobs j \n"
				+ "where j.state like 'FINISHED' and starttime between ? and ? and "
				+ "(j.JOB_IMPL like '" + JobImpl.PyLaiaTrainingJob + "' or j.JOB_IMPL like '" + JobImpl.PyLaiaTrainingJobLarge + "')";
		try (Connection conn = DbConnection.getConnection()) {
			PreparedStatement prep = conn.prepareStatement(SQL);
			prep.setLong(1, from.getTime());
			prep.setLong(2, to.getTime());
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				totalTrainingTime = " " + hourFormat(rs.getLong("job_runtime"));
			}
		}
	}

	private void getRunningTotals() throws SQLException {
		String SQL = "select count(htr_id) as nr_of_models from htr where type = 'text'";
		try (Connection conn = DbConnection.getConnection()) {
			PreparedStatement prep = conn.prepareStatement(SQL);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				runningTotals += "\nTotal HTR Models : " + rs.getInt("nr_of_models");
				jo.put("totalHtrModels", rs.getInt("nr_of_models"));
			}
		}
		SQL = "select count(*) as nr_of_images from images";
		try (Connection conn = DbConnection.getConnection()) {
			PreparedStatement prep = conn.prepareStatement(SQL);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				runningTotals += "\nTotal Images : " + rs.getInt("nr_of_images");
				jo.put("totalImages", rs.getInt("nr_of_images"));
			}
		}
		try (Connection conn = DbConnection.getConnection()) {
			SQL = "select count(*) as nr_of_images from images\n"
					+ "join pages on IMAGES.IMAGE_ID = PAGES.IMAGE_ID join jobs on PAGES.DOCID = jobs.DOCID join session_history on jobs.SESSION_HISTORY_ID = session_history.SESSION_HISTORY_ID\n"
					+ "where session_history.USERAGENT like '%Android%'";
			PreparedStatement prep = conn.prepareStatement(SQL);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				runningTotals += "\nTotal DocScan uploaded Images : " + rs.getInt("nr_of_images");
				jo.put("totalDocScanUploads", rs.getInt("nr_of_images"));
			}
		}
	}

	public void getHtrModelCreated(Date from, Date to) throws SQLException {
		String SQL = "select count(htr_id) as nr_of_models, sum(nr_of_words) as nr_of_words from htr where type = 'text' and created between ? and ?";
		try (Connection conn = DbConnection.getConnection()) {
			PreparedStatement prep = conn.prepareStatement(SQL);
			prep.setDate(1, new java.sql.Date(from.getTime()));
			prep.setDate(2, new java.sql.Date(to.getTime()));
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				htrModelsCreated = "" + rs.getInt("nr_of_models") + " | Words : " + rs.getInt("nr_of_words");
				jo.put("htrModelsCreated", rs.getInt("nr_of_models"));
				jo.put("htrModelsCreatedWords", rs.getInt("nr_of_words"));
			}
		}
	}

	public void getCreditStats(Date from, Date to) throws SQLException {
		String nrOfOrdersSql = "select count(distinct order_id) from credit_packages "
				+ "where purchase_date between ? and ?";
		countShopOrders = queryForDecimalValue(nrOfOrdersSql, from, to).intValue();

		String nrOfPackagesSql = "select count(package_id) from credit_packages "
				+ "where order_id is not null and purchase_date between ? and ?";
		countCreditPackages = queryForDecimalValue(nrOfPackagesSql, from, to).intValue();

		String nrOfCreditsSql = "select sum(p.nr_of_credits) from credit_packages c "
				+ "join credit_products p on p.product_id = c.product_id "
				+ "where c.order_id is not null and c.purchase_date between ? and ?";
		countCredits = queryForDecimalValue(nrOfCreditsSql, from, to).doubleValue();

		// create top ten ranking
		int productCount = 10;
		creditProductRanking = getCreditProductRanking(productCount, from, to);

		freeCreditConsumption = getCreditConsumption(true, from, to);
		paidCreditConsumption = getCreditConsumption(false, from, to);
	}

	public List<Pair<Integer, String>> getCreditProductRanking(int nrOfResults, Date from, Date to)
			throws SQLException {
		String productRankingSql = "select count(c.order_id), p.label from credit_packages c "
				+ "join credit_products p on p.product_id = c.product_id "
				+ "where c.order_id is not null and c.purchase_date between ? and ? " + "group by p.label "
				+ "order by count(c.order_id) desc";
		List<Pair<Integer, String>> creditProductRanking = new ArrayList<>();
		try (Connection conn = DbConnection.getConnection()) {
			PreparedStatement prep = conn.prepareStatement(productRankingSql);
			prep.setFetchSize(nrOfResults);
			prep.setDate(1, new java.sql.Date(from.getTime()));
			prep.setDate(2, new java.sql.Date(to.getTime()));
			ResultSet rs = prep.executeQuery();
			while (rs.next() && nrOfResults-- > 0) {
				Pair<Integer, String> item = Pair.of(rs.getInt(1), rs.getString(2));
				creditProductRanking.add(item);
			}
		}
		return creditProductRanking;
	}

	public List<TrpCreditTransaction> getCreditConsumption(boolean isPackageFreeOfCharge,
			Date from, Date to) throws SQLException {
		String filter;
		if (isPackageFreeOfCharge) {
			filter = "p.is_free_of_charge = 1 ";
		} else {
			filter = "(p.is_free_of_charge is null or p.is_free_of_charge = 0) ";
		}

		String sql = "select r.job_impl, p.is_free_of_charge, sum(t.credit_value) as consumed_credits, sum(t.nr_of_pages) as processed_pages from credit_transactions t "
				+ "join credit_packages p on p.package_id = t.package_id "
				+ "join credit_costs c on c.costs_id = t.costs_id "
				+ "join job_impl_registry r on r.job_impl_registry_id = c.job_impl_registry_id "
				+ "where t.time between ? and ? " + "and " + filter + " "
				+ "group by r.job_impl, p.is_free_of_charge "
				+ "order by r.job_impl";
		List<TrpCreditTransaction> transactions = new ArrayList<>();
		try (Connection conn = DbConnection.getConnection()) {
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setDate(1, new java.sql.Date(from.getTime()));
			prep.setDate(2, new java.sql.Date(to.getTime()));
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				TrpCreditTransaction t = new TrpCreditTransaction();
				t.setDescription(rs.getString("job_impl"));
				t.setCreditValue(rs.getDouble("consumed_credits"));
				// FIXME CreditManager till persistence 1.13.4 used double for calculation which
				// introduced a rounding error!
				// remove rounding once the report timespan numbers are clean
				t.setNrOfPages(BigDecimal.valueOf(Math.round(rs.getDouble("processed_pages"))).doubleValue());
				transactions.add(t);
			}
		}
		return transactions;
	}

	public static BigDecimal queryForDecimalValue(final String sql, Date from, Date to)
			throws SQLException {
		long start = System.currentTimeMillis();
		try (Connection conn = DbConnection.getConnection()) {
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setDate(1, new java.sql.Date(from.getTime()));
			prep.setDate(2, new java.sql.Date(to.getTime()));
			ResultSet rs = prep.executeQuery();
			logger.debug("Query executed in {} ms: {}", System.currentTimeMillis() - start, sql);
			if (!rs.next()) {
				logger.warn("Query returned no value: {}", sql);
				return BigDecimal.valueOf(0);
			} else {
				BigDecimal value = rs.getBigDecimal(1);
				return value == null ? BigDecimal.valueOf(0) : value;
			}
		}
	}

	public static BigDecimal queryForDecimalValue(final String sql, long fromTime, long toTime)
			throws SQLException {
		long start = System.currentTimeMillis();
		try (Connection conn = DbConnection.getConnection()) {
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setLong(1, fromTime);
			prep.setLong(2, toTime);
			ResultSet rs = prep.executeQuery();
			logger.debug("Query executed in {} ms: {}", System.currentTimeMillis() - start, sql);
			if (!rs.next()) {
				logger.warn("Query returned no value: {}", sql);
				return BigDecimal.valueOf(0);
			} else {
				BigDecimal value = rs.getBigDecimal(1);
				return value == null ? BigDecimal.valueOf(0) : value;
			}
		}
	}

	public void generateReport(List<String> recipients, Date from, Date to, String timespanLabel) {
		try {
			getJobTime(from, to, HTR_MODULE);

			getJobTime(from, to, OCR_MODULE);

			getJobTime(from, to, LA_MODULE);

			getTotalJobTime(from, to, HTR_MODULE);

			getTotalJobTime(from, to, OCR_MODULE);

			getClientJobTime(from, to, HTR_MODULE, LITECLIENT);

			getClientJobTime(from, to, UPLOAD, LITECLIENT);

			getClientJobTime(from, to, HTR_MODULE, EXPERTCLIENT);

			getClientJobTime(from, to, UPLOAD, EXPERTCLIENT);

			getTotalJobTime(from, to, LA_MODULE);

			getTrainingTotalTime(from, to);

			getHtrModelCreated(from, to);

			getTrainingTime(from, to);

			getRunningTotals();

			getCreditStats(from, to);

			emailMessage(from, to);

			sendReportMail(recipients, null, from, to, timespanLabel);
		} catch (Exception e) {
			logger.error("Reporting failed!", e);
		}
	}

	static public List<String> mailingList() {
		TrpProperties mailProp = new TrpProperties("email.properties");
		List<String> mailListProp = mailProp.getCsvStringListProperty("recipients", false);
		logger.debug("Sending mail to {}", mailListProp);
		return mailListProp;
	}

	private static String hourFormat(long l) {
		return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(l),
				TimeUnit.MILLISECONDS.toMinutes(l) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(l)),
				TimeUnit.MILLISECONDS.toSeconds(l) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(l)));
	}

	public static void main(String[] args) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				DbConnection.shutDown();
			}
		});
		List<String> recipients = mailingList();

		if(args.length < 1) {
			logger.error("Usage: ReportFromDatabase <reportTime> <dbConfig, default=Prod>");
			Runtime.getRuntime().exit(1);
		}

		Integer reportTime = Integer.parseInt(args[0]);
		ReportTimespan timespan = ReportTimespan.createTimespan(reportTime);

		DbConfig dbConfig = DbConfig.Prod;
		if(args.length > 1) {
			try {
				dbConfig = DbConfig.valueOf(args[1]);
			} catch (IllegalArgumentException e) {
				logger.error("Usage: ReportFromDatabase <reportTime> <dbConfig, default=Prod>");
				logger.error("Invalid dbConfig: {}", args[1]);
				Runtime.getRuntime().exit(1);
			}
		}
		DbConnection.setConfig(dbConfig);

		ReportFromDatabase report = new ReportFromDatabase();
		report.generateReport(recipients, timespan.getFrom(), timespan.getTo(), timespan.getTimespanLabel());

		Runtime.getRuntime().exit(0);
	}

}
