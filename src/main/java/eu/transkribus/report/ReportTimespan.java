package eu.transkribus.report;

import eu.transkribus.core.util.DateUtils;

import java.util.Calendar;
import java.util.Date;

public class ReportTimespan {

    final Date from;
    final Date to;
    final String timespanLabel;

    private ReportTimespan(Date from, Date to, String timespanLabel) {
        this.from = from;
        this.to = to;
        this.timespanLabel = timespanLabel;
    }

    public Date getFrom() {
        return from;
    }

    public Date getTo() {
        return to;
    }

    public String getTimespanLabel() {
        return timespanLabel;
    }

    @Override
    public String toString() {
        return "ReportTimespan{" +
                "from=" + from +
                ", to=" + to +
                ", timespanLabel='" + timespanLabel + '\'' +
                '}';
    }

    /**
     * Create a timespan for a given report time legacy CLI arguments
     * @param reportTime 1, 7, 30, 365 are allowed values
     * @return daily, weekly, monthly or yearly timespan
     */
    public static ReportTimespan createTimespan(int reportTime) {
        final String timespanLabel;
        Date from;
        Date to;
        Calendar cal;
        switch (reportTime) {
            case 1:
                timespanLabel = "Daily";
                to = DateUtils.getCalendarStartOfToday().getTime();
                cal = DateUtils.getCalendarStartOfToday();
                cal.add(Calendar.DAY_OF_MONTH, -1);
                from = cal.getTime();
                break;
            case 7:
                timespanLabel = "Weekly";
                to = DateUtils.getCalendarStartOfToday().getTime();
                cal = DateUtils.getCalendarStartOfToday();
                cal.add(Calendar.DAY_OF_MONTH, -7);
                from = cal.getTime();
                break;
            case 30:
                timespanLabel = "Monthly";
                to = DateUtils.getCalendarStartOfMonth().getTime();
                cal = DateUtils.getCalendarStartOfMonth();
                cal.add(Calendar.MONTH, -1);
                from = cal.getTime();
                break;
            case 365:
                timespanLabel = "Yearly";
                to = DateUtils.getCalendarStartOfYear().getTime();
                cal = DateUtils.getCalendarStartOfYear();
                cal.add(Calendar.YEAR, -1);
                from = cal.getTime();
                break;
            default:
                throw new IllegalArgumentException("Unknown report time argument: " + reportTime);
        }
        return new ReportTimespan(from, to, timespanLabel);
    }

    public static ReportTimespan createTimespanForYear(int year) {
        final String timespanLabel = year + " Yearly";
        Calendar calTo = DateUtils.getCalendarStartOfYear();
        calTo.set(Calendar.YEAR, year);
        calTo.add(Calendar.YEAR, 1);

        Calendar calFrom = DateUtils.getCalendarStartOfYear();
        calFrom.set(Calendar.YEAR, year);
        return new ReportTimespan(calFrom.getTime(), calTo.getTime(), timespanLabel);
    }

    public static ReportTimespan createTimespanForMonth(int year, int month) {
        final String timespanLabel = year + "-" + month + " Monthly";
        Calendar calTo = DateUtils.getCalendarStartOfYear();
        calTo.set(Calendar.YEAR, year);
        calTo.set(Calendar.MONTH, month);

        Calendar calFrom = DateUtils.getCalendarStartOfYear();
        calFrom.set(Calendar.YEAR, year);
        calFrom.set(Calendar.MONTH, month - 1);
        return new ReportTimespan(calFrom.getTime(), calTo.getTime(), timespanLabel);
    }
}
