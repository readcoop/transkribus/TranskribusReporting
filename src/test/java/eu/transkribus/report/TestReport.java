package eu.transkribus.report;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.transkribus.persistence.DbConnection;

public class TestReport {
	private static final Logger logger = LoggerFactory.getLogger(TestReport.class);

	@Test
	@Ignore("Takes very long, therefore ignored by default.")
	public void testReportIntervals() {
		int[] timespanDays = { 1, 7, 30, 365 };
		List<String> recipients = Collections.singletonList("p.kahle@readcoop.eu");
		for(int timespan : timespanDays) {
			ReportFromDatabase report = new ReportFromDatabase();
			long start = System.currentTimeMillis();
			ReportTimespan reportTimespan = ReportTimespan.createTimespan(timespan);
			report.generateReport(recipients, reportTimespan.getFrom(), reportTimespan.getTo(), reportTimespan.getTimespanLabel());
			logger.info("Generated and sent report for {} days in {} seconds", timespan, (System.currentTimeMillis() - start) / 1000);
		}
	}

	@Test
	@Ignore("Takes very long, therefore ignored by default.")
	public void testYearlyReports() {
		List<String> recipients = Collections.singletonList("p.kahle@readcoop.eu");
		for(int year = 2014; year < 2025; year++) {
			ReportFromDatabase report = new ReportFromDatabase();
			long start = System.currentTimeMillis();
			ReportTimespan reportTimespan = ReportTimespan.createTimespanForYear(year);
			logger.debug("Creating report for timespan: {}", reportTimespan);
			report.generateReport(recipients, reportTimespan.getFrom(), reportTimespan.getTo(), reportTimespan.getTimespanLabel());
			logger.info("Generated and sent {} report in {} seconds", reportTimespan.getTimespanLabel(), (System.currentTimeMillis() - start) / 1000);
		}
	}

	@Test
	@Ignore("Takes very long, therefore ignored by default.")
	public void testMonthlyReports() {
		List<String> recipients = Collections.singletonList("p.kahle@readcoop.eu");
		final int year = 2019;
		for(int month = 12; month <= 12; month++) {
			ReportFromDatabase report = new ReportFromDatabase();
			long start = System.currentTimeMillis();
			ReportTimespan reportTimespan = ReportTimespan.createTimespanForMonth(year, month);
			logger.debug("Creating report for timespan: {}", reportTimespan);
			report.generateReport(recipients, reportTimespan.getFrom(), reportTimespan.getTo(), reportTimespan.getTimespanLabel());
			logger.info("Generated and sent {} report in {} seconds", reportTimespan.getTimespanLabel(), (System.currentTimeMillis() - start) / 1000);
		}
	}

	@Test
	public void testCreditStats() throws SQLException {
		try {
			ReportFromDatabase report = new ReportFromDatabase();
			ReportTimespan timespan = ReportTimespan.createTimespan(1);
			report.getCreditStats(timespan.getFrom(), timespan.getTo());
			logger.info("Counts: {} {} {} {}", 
					report.countCreditPackages,
					report.countCredits,
					report.countShopOrders,
					report.creditProductRanking);
		} catch (SQLException e) {
			logger.error("Gathering credits stats failed!", e);
			throw e;
		} finally {
			DbConnection.shutDown();
		}
	}
}
